import {IPersist, IOptions} from './persist-interface';

export class LocalPersist implements IPersist {

    prefix: string;
    store: object;
    timeouts: object;

    constructor(options: IOptions) {
        this.prefix = (options.prefix || '').toLowerCase().replace(/[^a-z0-9]+/gi, '');
        this.store = {};
        this.timeouts = {};
    }

    __namespaceKey(key: string) {
        return `${this.prefix }_${key}`;
    }

    _setTimeout(my_key: string, timeout: number) {
        let self = this;
        if (self.timeouts[my_key]) {
            clearTimeout(self.timeouts[my_key]);
            delete self.timeouts[my_key];
        }
        this.timeouts[my_key] = setTimeout(() => {
            delete self.store[my_key];
            delete self.timeouts[my_key];
        }, timeout * 1000);
    }

    set(key: string, value: any, timeout: number = 60): Promise<any> {
        let self = this;
        let my_key = self.__namespaceKey(key);

        return new Promise((resolve, reject) => {
            try {
                self.store[my_key] = value;
                self._setTimeout(my_key, timeout);
                resolve(value);
            }
            catch (err) {
                reject(err);
            }
        });
    }

    get(key: string): Promise<any> {
        let self = this;
        let my_key = self.__namespaceKey(key);

        return new Promise((resolve, reject) => {
            if (self.store.hasOwnProperty(my_key)) {
                if (self.store[my_key] === null) {
                    reject();
                }
                resolve(self.store[my_key]);
            }
            else {
                reject();
            }
        });
    }

    delete(key): Promise<any> {
        let self = this;
        let my_key = self.__namespaceKey(key);
        return new Promise((resolve, reject) => {
            if (self.store.hasOwnProperty(my_key)) {
                delete self.store[my_key];
                resolve();
            }
            else {
                reject();
            }
        });
    }


    lock(key: string) {
        let self = this;
        return new Promise((resolve, reject) => {
            self.get(`lock_${key}`)
                .then(() => {
                    self.lock(key);
                })
                .catch(() => {
                    self.set(`lock_${key}`, true)
                        .then(() => {
                            resolve();
                        })
                        .catch(() => {
                            self.lock(key);
                        });
                });
        })
    }

    unlock(key: string) {
        return this.delete(`lock_${key}`);
    }

    update(key: string, value: any, timeout: number) {
        return this.set(key, value, timeout);
    }

    touch(key: string, timeout: number = 60) {
        let self = this;
        let my_key = self.__namespaceKey(key);
        return new Promise((resolve, reject) => {
            if (self.store.hasOwnProperty(my_key)) {
                self._setTimeout(my_key, timeout);
                resolve();
            }
            else {
                reject();
            }
        });
    }

    // return stored keys
    keys(){
        return Object.keys(this.store);
    }
}

