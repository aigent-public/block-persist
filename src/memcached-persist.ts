import * as Memcached from 'memcached';
import {IPersist, IOptions} from './persist-interface';

export class MemcachedPersist implements IPersist {

    prefix: string;
    memcached: Memcached;

    constructor(options: IOptions) {
        this.prefix = (options.prefix || '').toLowerCase().replace(/[^a-z0-9]+/gi, '');
        this.memcached = new Memcached(options.hosts || ['127.0.0.1:11211'], {});
    }

    set(key: string, value: any, timeout: number = 60): Promise<any> {
        let self = this;
        return new Promise((resolve, reject) => {
            try {
                self.memcached.set(`${self.prefix }_${key}`, value, timeout, (err: any) => {
                    if (err) reject(err);
                    else resolve(value);
                });
            }
            catch (err) {
                reject(err);
            }
        })
    }

    get(key: string): Promise<any> {
        let self = this;
        return new Promise((resolve, reject) => {
            try {
                self.memcached.get(`${self.prefix }_${key}`, (err: any, value: any) => {
                    if (err) reject(err);
                    else {
                        if (typeof value === 'undefined' || value === null) reject();
                        else resolve(value);
                    }
                });
            }
            catch (err) {
                reject(err);
            }
        });
    }

    delete(key: string): Promise<any> {
        let self = this;
        return new Promise((resolve, reject) => {
            try {
                self.memcached.del(`${self.prefix }_${key}`, (err: any) => {
                    if (err) reject(err);
                    else resolve();
                });
            }
            catch (err) {
                reject(err);
            }
        })
    }

    lock(key: string) {
        let self = this;
        return new Promise((resolve, reject) => {
            self.get(`lock_${key}`)
                .then(() => {
                    self.lock(key);
                })
                .catch(() => {
                    self.set(`lock_${key}`, true)
                        .then(() => {
                            resolve();
                        })
                        .catch(() => {
                            self.lock(key);
                        });
                });
        })
    }

    unlock(key: string) {
        return this.delete(`lock_${key}`);
    }

    touch(key: string, timeout: number = 60) {
        let self = this;
        return new Promise((resolve, reject) => {
            self.memcached.touch(`${self.prefix }_${key}`, timeout, (err => {
                if (!err) {
                    resolve();
                } else {
                    reject(err);
                }
            }));
        });
    }

    update(key: string, value: any, timeout: number) {
        let self = this;
        return new Promise((resolve, reject) => {
            try {
                self.memcached.replace(`${self.prefix }_${key}`, value, timeout, (err: any) => {
                    if (err) reject(err);
                    else resolve(value);
                });
            }
            catch (err) {
                reject(err);
            }
        })
    }

    keys() {
        return [];
    }
}
