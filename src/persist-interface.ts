export interface IPersist {
    get: (key: string) => Promise<any>;
    touch: (key: string, timeout: number) => Promise<any>;
    set: (key: string, value: any, timeout: number) => Promise<any>;
    update: (key: string, value: any, timeout: number) => Promise<any>;
    delete: (key: string) => Promise<any>;
    lock: (key: string) => Promise<any>;
    unlock: (key: string) => Promise<any>;
    keys: () => string[];
}

export type IOptions = {
    prefix: string;
    hosts?: string[];
}


