# block-persist


```
'use strict';

const Persist = require('block-persist');

const mc = Persist.memcached({
    prefix: 'test',
    hosts: ['memcached.int.eu.aigent.com:30389']
});

mc.set('key', 'some value', 5).then(data => {
    console.log('data stored');
}).catch(err => {
    console.error(err);
});

setInterval(() => {
    mc.get('key')
        .then(value => {
            console.debug('then:',value);
        })
        .catch(err => {
            console.error('catch:',err);
        });
}, 1000);
```


## Dev:

Transpile TypeScript into Javascript:

```
npm install
npm run-script build
```

