import {LocalPersist} from "./src/local-persist";
import {MemcachedPersist} from "./src/memcached-persist";
import {IOptions} from "./src/persist-interface";

export function local(options: IOptions) {
    return new LocalPersist(options);
}

export function memcached(options: IOptions) {
    return new MemcachedPersist(options);
}
