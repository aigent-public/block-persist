'use strict';

const Persist = require ('../dist/index');

const mc = Persist.memcached({
    prefix: 'test',
    hosts: ['memcached.int.eu.aigent.com:30389']
});

mc.set('key', 'some value', 60).then(data => {
    console.log('data stored');
}).catch(err => {
    console.error(err);
});


mc.set('key', 'some value', 60).then(data => {
    console.log('data stored');
}).catch(err => {
    console.error(err);
});

mc.set('key', 'some value', 60).then(data => {
    console.log('data stored');
}).catch(err => {
    console.error(err);
});


setInterval(() => {

    mc.keys();

    mc.get('key')
        .then(value => {
            console.debug('then:',value);
        })
        .catch(err => {
            console.error('catch:',err);
        });
}, 1000);